//
//  LoginViewController.swift
//  PlayAPP
//
//  Created by Wilson Li on 6/11/15.
//  Copyright (c) 2015 Wilson Li. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var dialogView: DesignableView!
    
    @IBAction func loginButtonDidTouch(sender: AnyObject) {
        
        dialogView.animation = "shake"
        dialogView.animate()
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func closeButtonDidTouch(sender: AnyObject) {
        
        dismissViewControllerAnimated(true, completion: nil)
        
        dialogView.animation = "zoomOut"
        dialogView.animate()
        
    }
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        view.endEditing(true)
    }

}
