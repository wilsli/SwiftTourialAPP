//
//  MenuViewController.swift
//  PlayAPP
//
//  Created by Wilson Li on 6/12/15.
//  Copyright (c) 2015 Wilson Li. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController {

    @IBOutlet weak var dialogView: DesignableView!
    
    @IBAction func closeButtonDidTouch(sender: AnyObject) {
        
        dismissViewControllerAnimated(true, completion: nil)
        
        dialogView.animation = "fall"
        dialogView.animate()
        
    }

    

}
