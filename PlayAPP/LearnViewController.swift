//
//  LearnViewController.swift
//  PlayAPP
//
//  Created by Wilson Li on 6/11/15.
//  Copyright (c) 2015 Wilson Li. All rights reserved.
//

import UIKit

class LearnViewController: UIViewController {

    @IBOutlet weak var dialogView: DesignableView!
    @IBOutlet weak var bookImageView: SpringImageView!
    
    @IBAction func learnButtonDidTouch(sender: AnyObject) {
        openURLinSafari("http://designcode.io")
    }
    
    @IBAction func closeButtonDidTouch(sender: AnyObject) {
        dialogView.animation = "fall"
        dialogView.animateNext{
            self.dismissViewControllerAnimated(true, completion: nil)
        }
    }
    
    @IBAction func twitterButtonDidTouch(sender: AnyObject) {
        openURLinSafari("https://twitter.com/cakel")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        
        dialogView.animate()
    }
    
    func openURLinSafari(url: String){
        let targetURL = NSURL(string: url)
        UIApplication.sharedApplication().openURL(targetURL!)
    }
    
    

}
