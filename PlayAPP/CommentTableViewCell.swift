//
//  CommentTableViewCell.swift
//  PlayAPP
//
//  Created by Wilson Li on 6/18/15.
//  Copyright (c) 2015 Wilson Li. All rights reserved.
//

import UIKit

class CommentTableViewCell: UITableViewCell {

    @IBOutlet weak var authorLabel: UILabel!
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var commentTextView: AutoTextView!
    @IBOutlet weak var upvoteButton: SpringButton!
    @IBOutlet weak var replyButton: SpringButton!

    @IBAction func upvoteButtonDidTouch(sender: AnyObject) {
    }
    
    @IBAction func replyButtonDidTouch(sender: AnyObject) {
    }
    
    func configureWithComment(comment: JSON) {
        
        let userPortraitUrl = comment["user_portrait_url"].string!
        let userDisplayName = comment["user_display_name"].string!
        let userJob = comment["user_job"].string!
        let createdAt = comment["created_at"].string!
        let voteCount = comment["vote_count"].int!
        let body = comment["body"].string!
        
        timeLabel.text = timeAgoSinceDate(dateFromString(createdAt, "yyyy-MM-dd'T'HH:mm:ssZ"), true)
        authorLabel.text = userDisplayName + ", " + userJob
        avatarImageView.image = UIImage(named: "content-avatar-default")
        upvoteButton.setTitle(toString(voteCount), forState: UIControlState.Normal)
        commentTextView.text = body
        
    }

}
